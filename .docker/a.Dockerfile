FROM python:3.9-buster

COPY requirements.txt /tmp/

RUN pip install --quiet --no-cache-dir -r /tmp/requirements.txt
