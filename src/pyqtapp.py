import sys
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QPalette, QColor


class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def set_color(self, customcolor: list):
        self.color = '#' + "".join([str(hex(i))[2:].zfill(2) for i in customcolor])

    def initUI(self):
        self.setWindowTitle('My First Application')
        self.move(300, 300)
        self.resize(400, 200)
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(QPalette.Background, QColor(self.color))  # TODO: This cause error
        self.setPalette(p)
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyApp()
    ex.set_color([255, 255, 255])
    sys.exit(app.exec_())
