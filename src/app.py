import sys
from easyprocess import EasyProcess
from pyvirtualdisplay.smartdisplay import SmartDisplay


class Take:
    """Take A Screen Shot of X11 Apps."""

    def __init__(self, app) -> None:
        self.app = app

    def launch(self):
        with SmartDisplay(backend='xephyr', visible=False, size=(400, 200)) as display:
            with EasyProcess([sys.executable, 'src/pyqtapp.py']):
                i = display.waitgrab()
                i.save('hellpo.png')


with SmartDisplay() as display:
    with EasyProcess([sys.executable, 'src/pyqtapp.py']):
        i = display.waitgrab()
    i.save('hellpo.png')
