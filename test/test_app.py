from pyats import aetest


@aetest.loop(uids=['three', 'fourth'])
class Testcase2(aetest.Testcase):
    @aetest.test.loop(destination=['a', 'b'])
    def test(self, destination):
        assert destination != 'a' or destination != 'b'


@aetest.loop(uids=['one', 'two'])
class Testcase(aetest.Testcase):
    @aetest.test.loop(destination=['a', 'b'])
    def test(self, destination):
        assert destination != 'a' or destination != 'b'
